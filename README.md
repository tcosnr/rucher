# Apiculteur en Bretagne

En 2016 la Bretagne tient une place significative dans le paysage de l'apiculture française, avec près de 4000 apiculteurs détenteurs d'environ 59000 ruches. 

## Apiculture 

L'apiculture est un secteur de l’agriculture qui réalise l'élevage d'abeilles à miel pour exploiter les produits de la ruche tels que le miel.

[apiculteur cotes d'armor](https://rucherdescosniers.fr)

## Parrainage de ruche

Parrainer une ruche c'est agir concrètement pour sauvegarder les abeilles et créer de nouvelles colonies d'abeilles dans des endroits sauvages et préservés. En remerciement de votre parrainage, les abeilles vous récoltez le fruit du travail des abeilles en recevant des pots de miel.

[parrainer une ruche](https://rucherdescosniers.fr/parrainage/)

## Récupération d'esssaims abeilles

Des abeilles se sont introduites dans votre jardin ou votre maison ? Vous avez sous les yeux un nid ou un essaim ? Ne tuez pas les abeilles et contactez un apiculteur qui viendra capturer les abeilles.

[abeilles lannion](https://rucherdescosniers.fr/)